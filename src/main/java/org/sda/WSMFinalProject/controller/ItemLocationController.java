package org.sda.WSMFinalProject.controller;

import org.hibernate.exception.ConstraintViolationException;
import org.sda.WSMFinalProject.model.Item;
import org.sda.WSMFinalProject.model.ItemLocation;
import org.sda.WSMFinalProject.model.StorageLocation;
import org.sda.WSMFinalProject.service.ItemLocationService;
import org.sda.WSMFinalProject.service.ItemService;
import org.sda.WSMFinalProject.service.StorageLocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/itemLocation")
public class ItemLocationController {
	
	@Autowired
	ItemLocationService itemLocationService;
	@Autowired
	ItemService itemService;
	@Autowired
	StorageLocationService storageLocationService;
	
	// Add item_location

			@RequestMapping(value = "/addNewItemLocation", method = RequestMethod.GET)
			public String addNewItemLocation(ModelMap model) {
				model.addAttribute("itemList", itemService.getAllItems());
				model.addAttribute("storageLocationList", storageLocationService.getAllStorageLocation());
				return "addNewItemLocation";
			}

			@RequestMapping(value = "/addNewItemLocation", method = RequestMethod.POST)
			public String addNewItemLocation(@RequestParam(value = "idItem") int item,
					@RequestParam(value = "idLocation") int location, @RequestParam(value = "quantity") int quantity, ModelMap model) {
				Item i = new Item();
				i.setIdItem(item);
				StorageLocation s = new StorageLocation();
				s.setIdStorageLocation(location);
				ItemLocation itemLocation = new ItemLocation(i, s, quantity);
				
					itemLocationService.addNewItemLocation(itemLocation);
				
				model.addAttribute("getAllItemLocations", itemLocationService.getAllItemLocation());

				return "getAllItemLocations";
			}

}
