package org.sda.WSMFinalProject.controller;

import org.sda.WSMFinalProject.model.Supplier;
import org.sda.WSMFinalProject.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/supplier")
public class SupplierController {

	@Autowired
	SupplierService supplierService;

	// Add supplier

	@RequestMapping(value = "/addNewSupplier", method = RequestMethod.GET)
	public String addNewSupplier() {
		return "addNewSupplier";
	}

	@RequestMapping(value = "/addNewSupplier", method = RequestMethod.POST)
	public String addNewSupplier(@RequestParam(value = "name") String supplierName,
			@RequestParam(value = "country") String country, @RequestParam(value = "city") String city,
			@RequestParam(value = "streetAndNumber") String streetAndNumber, ModelMap model) {
		Supplier supplier = new Supplier(supplierName, country, city, streetAndNumber);
		supplierService.addNewSupplier(supplier);
		model.addAttribute("listOfSuppliers", supplierService.getAllSuppliers());

		return "addNewSupplier";
	}

	// List suppliers

	@RequestMapping(value = "/getAllSuppliers", method = RequestMethod.GET)
	public String getAllSuppliers(ModelMap model) {
		model.addAttribute("getAllSuppliers", supplierService.getAllSuppliers());

		return "getAllSuppliers";
	}

	// Delete supplier

	@RequestMapping(value = "/deleteSupplier", method = RequestMethod.GET)
	public String removeSupplierById(@RequestParam("id") int idSupplier, ModelMap model) {

		supplierService.removeSupplierById(idSupplier);
		model.addAttribute("getAllSuppliers", supplierService.getAllSuppliers());
		model.addAttribute("deleteSupplier", "Customer :" + idSupplier + "was deleted");

		return "getAllSuppliers";
	}

	// Get supplier info

	@RequestMapping(value = "/updateSupplierById/{id}", method = RequestMethod.GET)
	public String updateCustomer(@ModelAttribute("supplier") Supplier supplier, ModelMap model,
			@PathVariable("id") int id) {

		Supplier infoSupplier = supplierService.getSupplierById(id);
		model.addAttribute("supplierUpdateInfo", infoSupplier);

		return "updateSupplier";
	}

	// Update supplier
	@RequestMapping(value = "/updateSupplier", method = RequestMethod.POST)
	public String updateSupplier(@ModelAttribute("supplier") Supplier supplier, ModelMap model) {

		supplierService.updateSupplier(supplier);
		model.addAttribute("getAllSuppliers", supplierService.getAllSuppliers());
		return "getAllSuppliers";
	}

}
