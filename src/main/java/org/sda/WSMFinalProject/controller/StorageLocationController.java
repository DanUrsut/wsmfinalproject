package org.sda.WSMFinalProject.controller;

import org.sda.WSMFinalProject.model.StorageLocation;
import org.sda.WSMFinalProject.service.StorageLocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/storageLocation")
public class StorageLocationController {
	
	@Autowired
	StorageLocationService storageLocationService;
	
	// Add storage location

		@RequestMapping(value = "/addNewStorageLocation", method = RequestMethod.GET)
		public String addNewStorageLocation() {
			return "addNewStorageLocation";
		}

		@RequestMapping(value = "/addNewStorageLocation", method = RequestMethod.POST)
		public String addNewStorageLocation(@RequestParam(value = "name") String nameStorageLocation,
				 ModelMap model) {
			StorageLocation storageLocation = new StorageLocation(nameStorageLocation);
			storageLocationService.addNewStorageLocation(storageLocation);
			model.addAttribute("listOfStorageLocations", storageLocationService.getAllStorageLocation());

			return "addNewStorageLocation";
		}

		// List storage location

		@RequestMapping(value = "/getAllStorageLocations", method = RequestMethod.GET)
		public String getAllStorageLocations(ModelMap model) {
			model.addAttribute("getAllStorageLocations", storageLocationService.getAllStorageLocation());

			return "getAllStorageLocations";
		}

		// Delete storage location

		@RequestMapping(value = "/deleteStorageLocations", method = RequestMethod.GET)
		public String removeStorageLocationsById(@RequestParam("id") int idStorageLocation, ModelMap model) {

			storageLocationService.removeStorageLocationById(idStorageLocation);
			model.addAttribute("getAllStorageLocations", storageLocationService.getAllStorageLocation());
			model.addAttribute("deleteStorageLocations", "Storage location: " + idStorageLocation + "was deleted");

			return "getAllStorageLocations";
		}

		// Get storage location info

		@RequestMapping(value = "/updateStorageLocationsById/{id}", method = RequestMethod.GET)
		public String updateStorageLocations(@ModelAttribute("storageLocations") StorageLocation storageLocations, ModelMap model,
				@PathVariable("id") int id) {

			StorageLocation infoStorageLocations = storageLocationService.getStorageLocationById(id);
			model.addAttribute("storageLocationUpdateInfo", infoStorageLocations);

			return "updateStorageLocation";
		}

		// Update storage location
		@RequestMapping(value = "/updateStorageLocation", method = RequestMethod.POST)
		public String updateSupplier(@ModelAttribute("storageLocation") StorageLocation storageLocation, ModelMap model) {

			storageLocationService.updateStorageLocation(storageLocation);
			model.addAttribute("getAllStorageLocations", storageLocationService.getAllStorageLocation());

			return "getAllStorageLocations";
		}


}
