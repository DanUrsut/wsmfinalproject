package org.sda.WSMFinalProject.controller;

import org.hibernate.exception.ConstraintViolationException;
import org.sda.WSMFinalProject.model.Item;
import org.sda.WSMFinalProject.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/item")
public class ItemController {
	
	@Autowired
	ItemService itemService;
	
	// Add item

		@RequestMapping(value = "/addNewItem", method = RequestMethod.GET)
		public String addNewItem() {
			return "addNewItem";
		}

		@RequestMapping(value = "/addNewItem", method = RequestMethod.POST)
		public String addNewItem(@RequestParam(value = "ItemCode") String itemCode,
				@RequestParam(value = "description") String description, ModelMap model) {
			Item item = new Item(itemCode, description);
			try{
				itemService.addNewItem(item);
			}
			catch (ConstraintViolationException e) {
				model.addAttribute("message", "Item already exist!");
			}
			model.addAttribute("getAllItems", itemService.getAllItems());

			return "getAllItems";
		}

		// List items

		@RequestMapping(value = "/getAllItems", method = RequestMethod.GET)
		public String getAllItems(ModelMap model) {
			model.addAttribute("getAllItems", itemService.getAllItems());

			return "getAllItems";
		}

		// Delete item

		@RequestMapping(value = "/deleteItem", method = RequestMethod.GET)
		public String removeItemById(@RequestParam("id") int idItem, ModelMap model) {

			itemService.removeItemById(idItem);
			model.addAttribute("getAllItems", itemService.getAllItems());
			model.addAttribute("message", "Item :" + idItem + "was deleted");

			return "getAllItems";
		}

		// Get item info

		@RequestMapping(value = "/updateItemById/{id}", method = RequestMethod.GET)
		public String updateItem(@ModelAttribute("item") Item item, ModelMap model,
				@PathVariable("id") int id) {

			Item infoItem = itemService.getItemById(id);
			model.addAttribute("itemInfo", infoItem);

			return "updateItem";
		}

		// Update item
		@RequestMapping(value = "/updateItem", method = RequestMethod.POST)
		public String updateItem(@ModelAttribute("item") Item item, ModelMap model) {

			itemService.updateItem(item);
			model.addAttribute("getAllItems", itemService.getAllItems());
			return "getAllItems";
		}


}
