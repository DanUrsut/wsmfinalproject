package org.sda.WSMFinalProject.controller;

import org.sda.WSMFinalProject.DAO.UserDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;

@Controller
@RequestMapping("/user")
public class UserController {
	
	@Autowired
	UserDAO userDAO;
	
	@RequestMapping(value = "/addNewUser", method = RequestMethod.GET)
	public String addNewUser() {
		return "addNewUser";
	}
	
	@RequestMapping(value = "/addNewUser", method = RequestMethod.POST)
	public String addNewCustomer(@RequestParam(value = "name") String userName,
			@RequestParam(value = "password") String password, ModelMap model) {
		System.out.println("UserController - addUser");
		
		return "addNewUser";
	}
	
   
}
