package org.sda.WSMFinalProject.DAO;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.sda.WSMFinalProject.model.Item;
import org.sda.WSMFinalProject.model.ItemLocation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ItemLocationDAOImpl implements ItemLocationDAO{
	
	@Autowired
	private SessionFactory sessionFactory;

	public void addNewItemLocation(ItemLocation itemLocation) {
	sessionFactory.getCurrentSession().save(itemLocation);
		
	}

	public void removeItemLocationById(int id) {
		Session session = sessionFactory.getCurrentSession();
		ItemLocation itemLocation = new ItemLocation();
		itemLocation.setIdItemLocation(id);
		session.delete(itemLocation);
		
	}

	public void removeItemLocation(ItemLocation itemLocation) {
		sessionFactory.getCurrentSession().delete(itemLocation);
		
	}

	public List<ItemLocation> getAllItemLocation() {
		return (List<ItemLocation>) sessionFactory.getCurrentSession().createQuery("from ItemLocation").list();
	}

	public void updateItemLocation(ItemLocation itemLocation) {
		sessionFactory.getCurrentSession().update(itemLocation);
		
	}

	public ItemLocation getItemLocationById(int idItemLocation) {
		return (ItemLocation) sessionFactory.getCurrentSession().get(ItemLocation.class, idItemLocation);
	}

}
