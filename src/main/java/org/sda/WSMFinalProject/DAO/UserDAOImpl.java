package org.sda.WSMFinalProject.DAO;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.sda.WSMFinalProject.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class UserDAOImpl implements UserDAO{

	@Autowired
	private SessionFactory sessionFactory;
	
	public User getUserByUsername(String username) {
Session session = sessionFactory.getCurrentSession();
		
		Query q = session.createQuery("from User where userName = :name");
		q.setParameter("name", username);

		return (User) q.uniqueResult();

		}

}
