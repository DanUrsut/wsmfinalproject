package org.sda.WSMFinalProject.DAO;

import org.sda.WSMFinalProject.model.User;

public interface UserDAO {
	
	public User getUserByUsername(String username);

}
