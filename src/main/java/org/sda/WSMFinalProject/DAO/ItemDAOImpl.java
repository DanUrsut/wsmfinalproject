package org.sda.WSMFinalProject.DAO;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.sda.WSMFinalProject.model.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ItemDAOImpl implements ItemDAO {

	@Autowired
	private SessionFactory sessionFactory;

	public void addNewItem(Item item) {
		sessionFactory.getCurrentSession().save(item);
	}

	public void removeItemById(int id) {
		Session session = sessionFactory.getCurrentSession();
		Item idItem = new Item();
		idItem.setIdItem(id);
		session.delete(idItem);

	}

	public void removeItem(Item item) {
		sessionFactory.getCurrentSession().delete(item);
	}

	public List<Item> getAllItems() {
		return (List<Item>) sessionFactory.getCurrentSession().createQuery("from Item").list();

	}

	public void updateItem(Item item) {
		sessionFactory.getCurrentSession().update(item);
	}

	@Override
	public Item getItemById(int idItem) {
		return (Item) sessionFactory.getCurrentSession().get(Item.class, idItem);
	}

}
