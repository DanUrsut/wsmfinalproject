package org.sda.WSMFinalProject.DAO;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.sda.WSMFinalProject.model.Supplier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class SupplierDAOImpl implements SupplierDAO {

	@Autowired
	private SessionFactory sessionFactory;

	public void addNewSupplier(Supplier supplier) {
		Session session = sessionFactory.getCurrentSession();
		session.save(supplier);
	}

	public void removeSupplier(Supplier supplier) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(supplier);

	}

	public void removeSupplierById(int id) {
		Session session = sessionFactory.getCurrentSession();

		Supplier idSupplier = new Supplier();
		idSupplier.setIdSupplier(id);
		session.delete(idSupplier);

	}

	public List<Supplier> getAllSuppliers() {
		Session session = sessionFactory.getCurrentSession();
		List<Supplier> suppliers = session.createQuery("from Supplier").list();

		return suppliers;
	}

	public void updateSupplier(Supplier supplier) {
		Session session = sessionFactory.getCurrentSession();
		session.update(supplier);

		
	}

	public Supplier getSupplierById(int idSupplier) {
		return (Supplier) sessionFactory.getCurrentSession().get(Supplier.class, idSupplier);
		
		
	}
}