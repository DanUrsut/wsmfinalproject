package org.sda.WSMFinalProject.DAO;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.sda.WSMFinalProject.model.StorageLocation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class StorageLocationDAOImpl implements StorageLocationDAO {

	@Autowired
	private SessionFactory sessionFactory;

	public void addNewStorageLocation(StorageLocation storageLocation) {
		sessionFactory.getCurrentSession().save(storageLocation);
	}

	public void removeStorageLocationById(int id) {
		Session session = sessionFactory.getCurrentSession();

		StorageLocation idStorageLocation = new StorageLocation();
		idStorageLocation.setIdStorageLocation(id);
		session.delete(idStorageLocation);
	}

	public void removeStorageLocation(StorageLocation storageLocation) {
		Session session = sessionFactory.getCurrentSession();
		session.delete(storageLocation);

	}

	public List<StorageLocation> getAllStorageLocation() {
		Session session = sessionFactory.getCurrentSession();
		List<StorageLocation> storageLocations = session.createQuery("from StorageLocation").list();
		return storageLocations;
	}

	public void updateStorageLocation(StorageLocation storageLocation) {
		Session session = sessionFactory.getCurrentSession();
		session.update(storageLocation);
	}

	public StorageLocation getStorageLocationById(int idStorageLocation) {
		return (StorageLocation) sessionFactory.getCurrentSession().get(StorageLocation.class, idStorageLocation);
	}

}
