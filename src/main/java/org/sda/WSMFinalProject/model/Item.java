package org.sda.WSMFinalProject.model;


import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "item")
public class Item {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idItem;
	private String itemCode;
	private String description;
	@OneToMany(mappedBy="item")
	private Set<ItemLocation> itemLocations;

	public Item() {
	}

	public Item(String itemCode, String description) {
		this.itemCode = itemCode;
		this.description = description;
	}

	public int getIdItem() {
		return idItem;
	}

	public void setIdItem(int idItem) {
		this.idItem = idItem;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	public Set<ItemLocation> getItemLocations() {
		return itemLocations;
	}

	public void setItemLocations(Set<ItemLocation> itemLocations) {
		this.itemLocations = itemLocations;
	}

	@Override
	public String toString() {
		return "Item [idItem=" + idItem + ", itemCode=" + itemCode + ", description=" + description + "]";
	}

}
