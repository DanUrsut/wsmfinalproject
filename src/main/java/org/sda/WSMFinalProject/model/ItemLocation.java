package org.sda.WSMFinalProject.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "item_location")
public class ItemLocation {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="idItem_location")
	private int idItemLocation;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idItem")
	private Item item;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "idLocation")
	private StorageLocation storageLocation;
	private int quantity;

	public ItemLocation() {
		super();
	}

	public ItemLocation(Item item, StorageLocation storageLocation, int quantity) {
		this.item = item;
		this.storageLocation = storageLocation;
		this.quantity = quantity;
	}

	public int getIdItemLocation() {
		return idItemLocation;
	}

	public void setIdItemLocation(int idItemLocation) {
		this.idItemLocation = idItemLocation;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public StorageLocation getStorageLocation() {
		return storageLocation;
	}

	public void setStorageLocation(StorageLocation storageLocation) {
		this.storageLocation = storageLocation;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	@Override
	public String toString() {
		return "ItemLocation [idItemLocation=" + idItemLocation + ", item=" + item + ", storageLocation="
				+ storageLocation + ", quantity=" + quantity + "]";
	}

}
