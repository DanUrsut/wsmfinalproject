package org.sda.WSMFinalProject.model;


import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "storagelocation")
public class StorageLocation {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idStorage_location")
	private int idStorageLocation;
	@Column(name = "storage_locationName")
	private String nameStorageLocation;
	@OneToMany(mappedBy="storageLocation")
	private Set<ItemLocation> itemLocations;

	public StorageLocation() {
	}

	public StorageLocation(String nameStorageLocation) {
		this.nameStorageLocation = nameStorageLocation;
	}

	public int getIdStorageLocation() {
		return idStorageLocation;
	}

	public void setIdStorageLocation(int idStorageLocation) {
		this.idStorageLocation = idStorageLocation;
	}

	public String getNameStorageLocation() {
		return nameStorageLocation;
	}

	public void setNameStorageLocation(String nameStorageLocation) {
		this.nameStorageLocation = nameStorageLocation;
	}

	public Set<ItemLocation> getItemLocations() {
		return itemLocations;
	}

	public void setItemLocations(Set<ItemLocation> itemLocations) {
		this.itemLocations = itemLocations;
	}

	@Override
	public String toString() {
		return "StorageLocation [idStorageLocation=" + idStorageLocation + ", nameStorageLocation="
				+ nameStorageLocation + "]";
	}

}
