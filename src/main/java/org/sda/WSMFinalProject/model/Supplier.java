package org.sda.WSMFinalProject.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "supplier")
public class Supplier {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idSupplier;
	@Column(name = "name")
	private String supplierName;
	private String country;
	private String city;
	private String streetAndNumber;

	public Supplier() {
		super();
	}

	public Supplier(String supplierName, String country, String city, String streetAndNumber) {
		super();
		this.supplierName = supplierName;
		this.country = country;
		this.city = city;
		this.streetAndNumber = streetAndNumber;
	}

	
	public int getIdSupplier() {
		return idSupplier;
	}

	public void setIdSupplier(int idSupplier) {
		this.idSupplier = idSupplier;
	}

	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getStreetAndNumber() {
		return streetAndNumber;
	}

	public void setStreetAndNumber(String streetAndNumber) {
		this.streetAndNumber = streetAndNumber;
	}

	@Override
	public String toString() {
		return "Supplier [idSupplier=" + idSupplier + ", supplierName=" + supplierName + ", country=" + country
				+ ", city=" + city + ", streetAndNumber=" + streetAndNumber + "]";
	}

}
