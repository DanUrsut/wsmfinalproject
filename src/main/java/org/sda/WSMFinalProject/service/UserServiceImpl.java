package org.sda.WSMFinalProject.service;

import org.sda.WSMFinalProject.DAO.UserDAO;
import org.sda.WSMFinalProject.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDAO userDAO;

	public User getUserByUsername(String username) {
		return userDAO.getUserByUsername(username);
	}

	public UserServiceImpl() {
		super();
	}

	public UserServiceImpl(UserDAO userDAO) {
		super();
		this.userDAO = userDAO;
	}

	public UserDAO getUserDAO() {
		return userDAO;
	}

	public void setUserDAO(UserDAO userDAO) {
		this.userDAO = userDAO;
	}

}
