package org.sda.WSMFinalProject.service;

import java.util.List;

import org.sda.WSMFinalProject.model.StorageLocation;

public interface StorageLocationService {

	public void addNewStorageLocation(StorageLocation storageLocation);

	public void removeStorageLocationById(int id);

	public void removeStorageLocation(StorageLocation storageLocation);
	
	public List<StorageLocation> getAllStorageLocation();
	
	public void updateStorageLocation(StorageLocation storageLocation);
	
	public StorageLocation getStorageLocationById(int idStorageLocation);
	
}
