package org.sda.WSMFinalProject.service;

import java.util.List;

import org.sda.WSMFinalProject.model.Supplier;

public interface SupplierService {
	
	public void addNewSupplier(Supplier supplier);
	
	public void removeSupplierById(int id);

	public void removeSupplier(Supplier supplier);
	
	public List<Supplier> getAllSuppliers();
	
	public void updateSupplier(Supplier supplier);
	
	public Supplier getSupplierById(int idSupplier);

}
