package org.sda.WSMFinalProject.service;

import java.util.List;

import javax.transaction.Transactional;

import org.sda.WSMFinalProject.DAO.StorageLocationDAO;
import org.sda.WSMFinalProject.model.StorageLocation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class StorageLocationServiceImpl implements StorageLocationService{
	
	@Autowired
	StorageLocationDAO storageLocationDAO;

	public void addNewStorageLocation(StorageLocation storageLocation) {
		storageLocationDAO.addNewStorageLocation(storageLocation);
	}

	public void removeStorageLocationById(int id) {
		storageLocationDAO.removeStorageLocationById(id);
		
	}

	public void removeStorageLocation(StorageLocation storageLocation) {
		storageLocationDAO.removeStorageLocation(storageLocation);
		
	}

	public List<StorageLocation> getAllStorageLocation() {
		return storageLocationDAO.getAllStorageLocation();
	}

	public void updateStorageLocation(StorageLocation storageLocation) {
		storageLocationDAO.updateStorageLocation(storageLocation);
	}

	public StorageLocation getStorageLocationById(int idStorageLocation) {
		return storageLocationDAO.getStorageLocationById(idStorageLocation);
	}

	public StorageLocationServiceImpl(StorageLocationDAO storageLocationDAO) {
		super();
		this.storageLocationDAO = storageLocationDAO;
	}

	public StorageLocationDAO getStorageLocationDAO() {
		return storageLocationDAO;
	}

	public StorageLocationServiceImpl() {
		super();
	}

	public void setStorageLocationDAO(StorageLocationDAO storageLocationDAO) {
		this.storageLocationDAO = storageLocationDAO;
	}
	
	

}
