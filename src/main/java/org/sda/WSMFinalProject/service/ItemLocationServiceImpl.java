package org.sda.WSMFinalProject.service;

import java.util.List;

import javax.transaction.Transactional;

import org.sda.WSMFinalProject.DAO.ItemLocationDAO;
import org.sda.WSMFinalProject.model.ItemLocation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class ItemLocationServiceImpl implements ItemLocationService {

	@Autowired
	ItemLocationDAO itemLocationDAO;

	public void addNewItemLocation(ItemLocation itemLocation) {
		itemLocationDAO.addNewItemLocation(itemLocation);
	}

	public void removeItemLocationById(int id) {
		itemLocationDAO.removeItemLocationById(id);
	}

	public void removeItemLocation(ItemLocation itemLocation) {
		itemLocationDAO.removeItemLocation(itemLocation);
	}

	public List<ItemLocation> getAllItemLocation() {
		return itemLocationDAO.getAllItemLocation();
	}

	public void updateItemLocation(ItemLocation itemLocation) {
		itemLocationDAO.updateItemLocation(itemLocation);
	}

	public ItemLocation getItemLocationById(int idItemLocation) {
		return itemLocationDAO.getItemLocationById(idItemLocation);
	}

	public ItemLocationServiceImpl() {
		super();
	}

	public ItemLocationServiceImpl(ItemLocationDAO itemLocationDAO) {
		super();
		this.itemLocationDAO = itemLocationDAO;
	}

	public ItemLocationDAO getItemLocationDAO() {
		return itemLocationDAO;
	}

	public void setItemLocationDAO(ItemLocationDAO itemLocationDAO) {
		this.itemLocationDAO = itemLocationDAO;
	}

}
