package org.sda.WSMFinalProject.service;

import java.util.List;

import javax.transaction.Transactional;

import org.sda.WSMFinalProject.DAO.ItemDAO;
import org.sda.WSMFinalProject.model.Item;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class ItemServiceImpl implements ItemService {

	@Autowired
	ItemDAO itemDAO;

	public void addNewItem(Item item) {
		itemDAO.addNewItem(item);
	}

	public void removeItemById(int id) {
		itemDAO.removeItemById(id);
	}

	public void removeItem(Item item) {
		itemDAO.removeItem(item);
	}

	public List<Item> getAllItems() {
		return itemDAO.getAllItems();
	}

	public void updateItem(Item item) {
		itemDAO.updateItem(item);
	}

	public Item getItemById(int idItem) {
		return itemDAO.getItemById(idItem);
	}

	public ItemServiceImpl() {
		super();
	}

	public ItemServiceImpl(ItemDAO itemDAO) {
		super();
		this.itemDAO = itemDAO;
	}

	public ItemDAO getItemDAO() {
		return itemDAO;
	}

	public void setItemDAO(ItemDAO itemDAO) {
		this.itemDAO = itemDAO;
	}
	
	

}
