package org.sda.WSMFinalProject.service;

import java.util.List;

import org.sda.WSMFinalProject.model.Item;

public interface ItemService {
	
	public void addNewItem(Item item);

	public void removeItemById(int id);

	public void removeItem(Item	item);
	
	public List<Item> getAllItems();
	
	public void updateItem(Item item);
	
	public Item getItemById(int idItem);

}
