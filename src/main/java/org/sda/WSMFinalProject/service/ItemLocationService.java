package org.sda.WSMFinalProject.service;

import java.util.List;

import org.sda.WSMFinalProject.model.ItemLocation;

public interface ItemLocationService {
	
	public void addNewItemLocation(ItemLocation itemLocation);

	public void removeItemLocationById(int id);

	public void removeItemLocation(ItemLocation	itemLocation);
	
	public List<ItemLocation> getAllItemLocation();
	
	public void updateItemLocation(ItemLocation itemLocation);
	
	public ItemLocation getItemLocationById(int idItemLocation);

}
