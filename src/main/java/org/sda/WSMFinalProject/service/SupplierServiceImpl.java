package org.sda.WSMFinalProject.service;

import java.util.List;

import org.sda.WSMFinalProject.DAO.SupplierDAO;
import org.sda.WSMFinalProject.model.Supplier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class SupplierServiceImpl implements SupplierService {

	@Autowired
	SupplierDAO supplierDAO;

	public void addNewSupplier(Supplier supplier) {
		supplierDAO.addNewSupplier(supplier);
	}

	public void removeSupplierById(int id) {
		supplierDAO.removeSupplierById(id);
	}

	public void removeSupplier(Supplier supplier) {
		supplierDAO.removeSupplier(supplier);
	}

	public List<Supplier> getAllSuppliers() {
		return supplierDAO.getAllSuppliers();
	}

	public void updateSupplier(Supplier supplier) {
		supplierDAO.updateSupplier(supplier);
	}

	public Supplier getSupplierById(int idSupplier) {
		return supplierDAO.getSupplierById(idSupplier);
	}

	public SupplierServiceImpl() {
	}

	public SupplierServiceImpl(SupplierDAO supplierDAO) {
		this.supplierDAO = supplierDAO;
	}

	public SupplierDAO getSupplierDAO() {
		return supplierDAO;
	}

	public void setSupplierDAO(SupplierDAO supplierDAO) {
		this.supplierDAO = supplierDAO;
	}

}
