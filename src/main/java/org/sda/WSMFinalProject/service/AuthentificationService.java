package org.sda.WSMFinalProject.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.sda.WSMFinalProject.model.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@Transactional
public class AuthentificationService implements UserDetailsService {

	@Autowired
	private UserService userService;

	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		org.sda.WSMFinalProject.model.User domainUser = userService.getUserByUsername(username);
		
	return new User(domainUser.getUserName(), domainUser.getPassword(), getAuthorities(domainUser));
	}
	
	/**
	 * Method converts our database user roles into spring Authorities object
	 * @param user
	 * @return
	 */
	
	public Collection<? extends GrantedAuthority> getAuthorities(org.sda.WSMFinalProject.model.User user){
		System.out.println("AuthenticationService creating authorities for username: " + user.getUserName());
		List<GrantedAuthority> authorities =new ArrayList<GrantedAuthority>();
		for(Role role: user.getRoles()) {
			authorities.add(new SimpleGrantedAuthority(role.getRoleName()));
		}
		return authorities;
	}

}
