<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

	<h2>${valueFromApp}</h2>
	<c:if test="${not empty  deleteSupplier}">
	${deleteSupplier}
	</c:if>
	<!-- PRINT LIST -->
	<table border="1">
		<c:forEach items="${getAllStorageLocations}" var="iterator">

			<tr>
				<td><c:out value="${iterator.getIdStorageLocation()}"></c:out></td>
				<td><c:out value="${iterator.getNameStorageLocation()}"></c:out></td>
				<td><form
						action="http://localhost:8080/WSMFinalProject/storageLocation/deleteStorageLocations"
						method="get">
						<input type="hidden" value="${iterator.getIdStorageLocation()}"
							name="id"> <input type="submit"
							value="Delete storage location">
					</form></td>
				<td>
					<form
						action="${pageContext.request.contextPath}/storageLocation/updateStorageLocationsById/${iterator.getIdStorageLocation()}">
						<input class="editButton" type="submit" value="Update">
					</form>
			</tr>
		</c:forEach>

	</table>

</body>
</html>