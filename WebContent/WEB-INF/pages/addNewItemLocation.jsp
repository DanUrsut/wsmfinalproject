<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add new storage location</title>
</head>
<body>

	<form
		action="http://localhost:8080/WSMFinalProject/itemLocation/addNewItemLocation"
		method="post">

		Item name: <select name="idItem" >
			<c:forEach items="${itemList}" var="iterator">
				<option value="${iterator.getIdItem()}"><c:out value="${iterator.getItemCode()}"></c:out></option>
			</c:forEach>
		<br></select> <select name="idLocation">
			<c:forEach items="${storageLocationList}" var="iterator">
				<option value="${iterator.getIdStorageLocation()}"><c:out value="${iterator.getNameStorageLocation()}"></c:out></option>
			</c:forEach>
		<br></select> Quantity: <input type="number" name="quantity" placeholder="Quantity">

		<input type="submit" value="Add new item on location">
	</form>

</body>
</html>