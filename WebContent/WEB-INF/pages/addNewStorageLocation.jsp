<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add new storage location</title>
</head>
<body>

<form
		action="http://localhost:8080/WSMFinalProject/storageLocation/addNewStorageLocation"
		method="post">

		Storage location name: <input type="text" name="name" placeholder="Name">
		
		<input type="submit" value="Add new storage location">
	</form>

</body>
</html>