<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Supplier administration page</title>
</head>
<body>

	<h1>Add new supplier</h1>

	<form
		action="http://localhost:8080/WSMFinalProject/supplier/addNewSupplier"
		method="post">

		Supplier name: <input type="text" name="name" placeholder="Name">
		<br>Country:<input type="text" name="country"
			placeholder="Country"> <br> <br>City:<input
			type="text" name="city" placeholder="City"> <br> <br>Street
		& Number:<input type="text" name="streetAndNumber"
			placeholder="StreetAndNumber"> <br>
		<input type="submit" value="Add new supplier">
	</form>

</body>
</html>