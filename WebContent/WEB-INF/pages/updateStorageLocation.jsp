<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Update storage location</title>
</head>
<body>
	<spring:url value="/storageLocation/updateStorageLocation" var="saveURL"></spring:url>
	<form:form method="POST" action="${saveURL}"
		modelAttribute="storageLocationUpdateInfo">

		<table>
			<tr>
				<td><form:label path="idStorageLocation">ID:</form:label></td>
				<td><form:input path="idStorageLocation" readonly="true" /></td>
			</tr>
			
			<tr>
				<td><form:label path="nameStorageLocation">ID:</form:label></td>
				<td><form:input path="nameStorageLocation" /></td>
			</tr>

			<br>
			<input type="submit" value="Update">
		</table>
	</form:form>
</body>
</html>